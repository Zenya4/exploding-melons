package com.Zenya4.ExplodingMelons;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new EatListener(), this);
	}
	public final class EatListener implements Listener {
	@EventHandler(priority = EventPriority.HIGHEST)
	public void eatMelon(PlayerItemConsumeEvent event) {
    Player player = event.getPlayer();
    ItemStack melon = player.getInventory().getItemInMainHand();
    if (melon.getType() == Material.MELON) {
    	if (event.getItem().isSimilar(melon)) {
    	Location location = player.getLocation();
    	World world = player.getWorld();
    	Entity entity = world.spawnEntity(location, EntityType.PRIMED_TNT);
    	((TNTPrimed)entity).setFuseTicks(0);
    	}
    }
    }
    }
	@Override
	public void onDisable() {
	}
}
